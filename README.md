# Timelapse

Utilidad para captura de fotografias a intervalos con un dispositivo Andriod,  
utilizando [Termux](https://termux.com/), **ssh**, **mogrify** y **rsync**.  

## 2 scripts:
  * **timelapse_master** (opcional):
    - Controla el script timelapse_host por medio de **ssh**.
    - Permite importar imagenes del host.
  * **timelapse_host**:  
    - Realiza la capturas segun argumentos.
    - Este script debe correr en Termux (android).

## Fotografias a intervalo  

Registro fotografico cada ***'X'*** tiempo, durante ***'Y'*** tiempo o ***'Z'*** cantidad de imagenes.

Las imagenes se guardan con el nombre (agumento **-n**) como prefijo, seguido de la fecha.  
`[NOMBRE]-[AÑO]-[MES]-[DIA]_[HH]:[MM]:[SS].jpeg`  
>   ej. ` secuencia-2022-02-14_02:50:36.jpeg `  

Las imagenes se guardan en `~/BashCamera/[NOMBRE]_[DIA]-[MES]-[AÑO]_[HH]:[MM]/`
>   ej. ` secuencia-2022-02-14_02:50/ `  

```
📂️ BashCamera/
└── 📂️ secuencia_14-02-2022_02:50/
    └── 🖼️  secuencia_2022-02-14_02:50:36.jpeg
```
----
## Ayuda

`timelapse_master --help`
```
   Uso: timelapse [OPCS]

     -h, --help                    - Muestra información de ayuda.
     -C, --copia                   - Copia capturas del host en /tmp/BashCamera.
     -d, --destino [opcional]      - Directorio donde se guardan las capturas.
                                     /tmp/BashCamera por defecto.
     -H, --host                    - Nombre del host segun ~/.ssh/config.
     -n, --nombre                  - Nombre prefijo de imagenes.
     -i, --lapso                   - Tiempo entre capturas (segundos).
     -f, --fin                     - Fin de la secuencia (segundos).
     -c, --fotos                   - Cantidad de imagenes para finalizar (int).
                                     invalida el fin de secuencia por tiempo.
     -r, --rotar                   - Girar la imagen (90-180-270)
     -v, --version                 - Muestra la fecha de la versión.

   
   Ejemplos: 

       timelapse -H [host] -n [titulo] -r 90 -i 300 -f 18000

       Solicita al host fotografias cada 5 minutos durante 5 horas.
       Las imagenes son nombradas [titulo]_[AÑO]-[MES]-[DIA]_[HH:MM:SS]
       y se guardan en //data/data/com.termux/files/home/storage/dcim/BashCamera/[titulo]/
       Las imagenes se giran 90°.

       timelapse -C [host] -d [DESTINO(opcional)]

       Copia las capturas del host (rsync)
```
`timelapse_host --help`
```
   Uso: timelapse [OPCS]

     -h, --help                    - Muestra información de ayuda.
     -n, --nombre                  - Nombre prefijo de imagenes.
     -i, --lapso                   - Tiempo entre capturas (segundos).
     -f, --fin                     - Fin de la secuencia (segundos).
     -c, --fotos                   - Cantidad de imagenes para finalizar (int).
                                     invalida el fin de secuencia por tiempo.
     -r, --rotar                   - Modifica el angulo de la imagen (90-180-270)
     -v, --version                 - Muestra la fecha de la versión.

   
   Ejemplo: 

       timelapse -n [NOMBRE] -i 300 -f 18000 -r 90

       Toma fotografias cada 5 minutos durante 5 horas.
       Las imagenes son nombradas [NOMBRE]_[AÑO]-[MES]-[DIA]_[HH:MM:SS]
       Las imagenes se giran 90°.
```

> Icon: thanks to [Billy Brown](http://b.illbrown.com/)
