#!/data/data/com.termux/files/usr/bin/env bash

Progrm=timelapse
VersionStr='14-02-2022'
NOMBRE_BASE='timelapse'
DIR_BASE="/data/data/com.termux/files/home/storage/dcim/BashCamera/"
CONTADOR=0
T_ANTERIOR=0
T_FIN=0
ROTAR=0
ANGULO=0



Err(){
    printf 'ERROR: %s\n' "$2" 1>&2
    [ $1 -gt 0 ] && exit $1
}

Uso(){
    while read; do
        printf '%s\n' "$REPLY"
    done <<-EOF
        Uso: $Progrm [OPCS]

          -h, --help                    - Muestra información de ayuda.
          -n, --nombre                  - Nombre prefijo de imagenes.
          -i, --lapso                   - Tiempo entre capturas (segundos).
          -f, --fin                     - Fin de la secuencia (segundos).
          -c, --fotos                   - Cantidad de imagenes para finalizar (int).
                                          invalida el fin de secuencia por tiempo.
          -r, --rotar                   - Modifica el angulo de la imagen (90-180-270)
          -v, --version                 - Muestra la fecha de la versión.

        
        Ejemplo: 

            timelapse -n [NOMBRE] -i 300 -f 18000 -r 90

            Toma fotografias cada 5 minutos durante 5 horas.
            Las imagenes son nombradas [NOMBRE]_[AÑO]-[MES]-[DIA]_[HH:MM:SS]
            Las imagenes se giran 90°

EOF
}

if ! [ -n "$1" ]; then
    Err 1 "'$0' Debes ingresar opciones. \"ej: --help\""
fi

#while [ -n "$1" ]; do
while [[ $# -gt 0 ]]; do
    case $1 in
        -v|--version)
            printf '%s\n' "$VersionStr"; exit 0 ;;
        -h|--help)
            Uso; exit 0 ;;
        -n|--nombre)
            NOMBRE_BASE="$2"
            shift
            shift
            ;;
        -i|--lapso)
            INTERVALO="$2"
            shift
            shift
            ;;
        -f|--fin)
            T_FIN="$2"    
            shift
            shift
            ;;
        -c|--fotos)
            CONTADOR="$2" 
            shift
            shift
            ;;
        -r|--rotar)
            ROTAR=1
            ANGULO="$2"
            [ ${ANGULO} -eq 0 ] && Err 1 "El angulo de giro debe ser: 90 - 180 - 270"
            shift
            shift
            ;;
        -*|--*)
            Err 1 "Opción no valida: $1"  ;;
        *)
            Err 1 'Argumento(s) invalido(s).'  ;;
  esac
done

[ -n "${INTERVALO}" ] || Err 1 "Debes Ingresar un intervalo"
[ "${INTERVALO}" -lt 15 ] && Err 1 "Intervalo de capturas debe ser >= 15"
[ ${CONTADOR} -eq 0 ] && [ ${T_FIN} -eq 0 ] && Err 1 "Debes ingresar un limite (tiempo o cantidad) (--help)"

NOMBRE_DIR="${NOMBRE_BASE}_$(date +'%d-%m-%Y_%H:%M')"
DIR_BASE="${DIR_BASE}${NOMBRE_DIR}/"
mkdir -p "${DIR_BASE}"

captura(){
    CAPTURA="${DIR_BASE}${NOMBRE_BASE}_$(date +'%Y-%m-%d_%H:%M:%S').jpeg"
    termux-camera-photo "${CAPTURA}"
    # Puede depender de un argumento y otra funcion. Por ahora queda así
    if [ ${ROTAR} -gt 0 ]; then
        mogrify -rotate "${ANGULO}" "${CAPTURA}"
    fi
}

# Limite por cantidad de capturas
if [ "${CONTADOR}" -gt 0 ]; then
    while [ $CONTADOR -gt 0 ]; do
        T_ACTUAL=$(date "+%s")
        T_DIFF=$((T_ACTUAL-T_ANTERIOR))
        if [ $T_DIFF -ge $INTERVALO ]; then
            T_ANTERIOR=${T_ACTUAL}
            captura
            ((CONTADOR--))
        else
            sleep 0.20
        fi
    done
else
    # Limite por tiempo
    T_ACTUAL=$(date "+%s")
    T_FIN=$((T_ACTUAL+T_FIN))
    while [ ${T_ACTUAL} -le ${T_FIN} ]; do
        T_ACTUAL=$(date "+%s")
        T_DIFF=$((T_ACTUAL-T_ANTERIOR))
        if [ $T_DIFF -ge $INTERVALO ]; then
            T_ANTERIOR=${T_ACTUAL}
            captura
        else
            sleep 0.20
        fi

    done
fi

exit 0

#############################################
# PENDIENTE:                                #
#   VALIDACION                              #
#############################################
