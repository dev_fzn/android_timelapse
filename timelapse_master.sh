#!/usr/bin/env bash

Progrm=timelapse
VersionStr='14-02-2022'
NOMBRE_BASE='timelapse'
DIR_BASE="//data/data/com.termux/files/home"
DIR_PRGRM="${DIR_BASE}/.scripts/timelapse"
DIR_CAPT="${DIR_BASE}/storage/dcim/BashCamera"
DIR_DESTINO="/tmp"
CONTADOR=0
T_FIN=0
COPIA=0
ROTAR=0
ANGULO=0

Err(){
    printf 'ERROR: %s\n' "$2" 1>&2
    [ $1 -gt 0 ] && exit $1
}

Uso(){
    while read; do
        printf '%s\n' "$REPLY"
    done <<-EOF
        Uso: $Progrm [OPCS]

          -h, --help                    - Muestra información de ayuda.
          -C, --copia                   - Copia capturas del host en ${DIR_DESTINO}/BashCamera.
          -d, --destino [opcional]      - Directorio donde se guardan las capturas.
                                          ${DIR_DESTINO}/BashCamera por defecto.
          -H, --host                    - Nombre del host segun ~/.ssh/config.
          -n, --nombre                  - Nombre prefijo de imagenes.
          -i, --lapso                   - Tiempo entre capturas (segundos).
          -f, --fin                     - Fin de la secuencia (segundos).
          -c, --fotos                   - Cantidad de imagenes para finalizar (int).
                                          invalida el fin de secuencia por tiempo.
          -r, --rotar                   - Girar la imagen (90-180-270).
          -v, --version                 - Muestra la fecha de la versión.

        
        Ejemplos: 

            timelapse -H [host] -n [titulo] -r 90 -i 300 -f 18000

            Solicita al host fotografias cada 5 minutos durante 5 horas.
            Las imagenes son nombradas [titulo]_[AÑO]-[MES]-[DIA]_[HH:MM:SS]
            y se guardan en ${DIR_CAPT}/[titulo]/
            Las imagenes se giran 90°.

            timelapse -C [host] -d [DESTINO(opcional)]

            Copia las capturas del host (rsync)

EOF
}

Importar() {
    rsync -azh --info=progress2 "${MAQUINA}:${DIR_CAPT}" "${DIR_DESTINO}"
    exit 0
}

if ! [ -n "$1" ]; then
    Err 1 "'$0' Debes ingresar opciones. \"ej: --help\""
fi

while [[ $# -gt 0 ]]; do
    case $1 in
        -v|--version)
            printf '%s\n' "$VersionStr"; exit 0 ;;
        -h|--help)
            Uso; exit 0 ;;
        -C|--copia)
            MAQUINA="$2"
            COPIA=1
            shift
            shift
            ;;
        -d|--destino)
            DIR_DESTINO="$2"
            shift
            shift
            ;;
        -H|--host)
            MAQUINA="$2"
            shift
            shift
            ;;
        -n|--nombre)
            NOMBRE_BASE="$2"
            shift
            shift
            ;;
        -i|--lapso)
            INTERVALO="$2"
            shift
            shift
            ;;
        -f|--fin)
            T_FIN="$2"
            shift
            shift
            ;;
        -c|--fotos)
            CONTADOR="$2"
            shift
            shift
            ;;
        -r|--rotar)
            ROTAR=1
            [ "" = "$2" ] && Err 1 "El angulo de giro debe ser: 90 - 180 - 270"
            ANGULO=${2}
            [ ${ANGULO} -le 0 ] && Err 1 "El angulo de giro debe ser: 90 - 180 - 270"
            shift
            shift
            ;;
        -*|--*)
            Err 1 "Opción no valida: $1"  ;;
        *)
            Err 1 'Argumento(s) invalido(s).'  ;;
  esac
done


[ ${COPIA} -gt 0 ] && Importar
[ -n "${INTERVALO}" ] || Err 1 "Debes Ingresar un intervalo"
[ "${INTERVALO}" -lt 15 ] && Err 1 "Intervalo de capturas debe ser >= 15"
[ ${CONTADOR} -eq 0 ] && [ ${T_FIN} -eq 0 ] && Err 1 "Debes ingresar un limite (tiempo o cantidad) (--help)"

if [ "${CONTADOR}" -gt 0 ]; then
    # Limite por cantidad de capturas
    if [ "${ROTAR}" -gt 0 ]; then
        # Rotar imagen
        ssh -f "${MAQUINA}" "${DIR_PRGRM} -n ${NOMBRE_BASE} -i ${INTERVALO} -c ${CONTADOR} -r ${ANGULO}"
    else
        ssh -f "${MAQUINA}" "${DIR_PRGRM} -n ${NOMBRE_BASE} -i ${INTERVALO} -c ${CONTADOR}"
    fi
else
    # Limite por tiempo
    if [ "${ROTAR}" -gt 0 ]; then
        # Rotar imagen
        ssh -f "${MAQUINA}" "${DIR_PRGRM} -n ${NOMBRE_BASE} -i ${INTERVALO} -f ${T_FIN} -r ${ANGULO}"
    else
        ssh -f "${MAQUINA}" "${DIR_PRGRM} -n ${NOMBRE_BASE} -i ${INTERVALO} -f ${T_FIN}"
    fi
fi

#########################################################################
# PENDIENTE:                                                            #
#                                                                       #
#   CASE y Func : uso de ffmpeg >> timelapse.mp4                        #
#                                                                       #
#   LIMITE POR TIEMPO                                                   #
#   sufijo s(egundos); m(inutos); h(oras); d(dias); S(emanas); M(eses)  #
#                                                                       #
#   VALIDACION                                                          #
#                                                                       #
#########################################################################
